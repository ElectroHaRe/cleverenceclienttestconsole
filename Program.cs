﻿using Cleverence;
using Serilog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Threading.Tasks;

namespace CleverenceClientTestConsole
{
    class Program
    {
        private static Client client;
        private static Product product;
        private static Serii seriiRow;
        private static TransportnyeUpakovki upakovkiRow;
        private static Warehouse warehouse;
        private static ClientTester clientTester;
        private static Kontragenty kontragenty;
        private static ILogger logger;

        static Program()
        {
            logger = new LoggerConfiguration()
#if DEBUG
                .MinimumLevel.Verbose()
#else
                .MinimumLevel.Error()
#endif
                .WriteTo.Console().CreateLogger();

            client = new Client(new HttpClient() { Timeout = TimeSpan.FromMinutes(4) });
            client.BaseUrl = ConfigurationManager.AppSettings.Get("BaseUrl");
            clientTester = new ClientTester(client, logger);

            product = GenerateTestProduct();
            seriiRow = GenerateTestSerii();
            upakovkiRow = GenerateTestUpakovki();
            warehouse = GenerateTestWarehouse();
            kontragenty = GenerateTestKontragenty();
        }

        private static Product GenerateTestProduct()
        {
            return new Product()
            {
                Id = "00000000000000",
                Barcode = "00000000000000",
                Marking = "00000000000000",
                BasePackingId = "0000000a - 000a - 0a00 - 0000 - 00a00aaa00a0",
                Name = "Test_Product_AK",
                Packings = new List<Packing>()
                {
                    new Packing()
                    {
                        Id = "0000000a - 000a - 0a00 - 0000 - 00a00aaa00a0",
                        Name = "Test_Packing_AK",
                        UnitsQuantity = 1
                    }
                }
            };
        }

        private static Serii GenerateTestSerii()
        {
            return new Serii()
            {
                Uid = "Test_Serii_AK",
                ImyaSerii = "0000000000 до 01.01.01",
                KlyuchSerij = "a585a000-a000-00a0-aa00-00a00a0aa00a",
                DataRozliva = new DateTimeOffset?(new DateTime(2001, 1, 1, 0, 0, 0)),
                NaimenovanieDlyaPoiska = "0000000000 до 01.01.01",
                DataSerii = new DateTimeOffset?(new DateTime(2001, 1, 1, 0, 0, 0)),
                IndeksSort = 0,
                Shtrihkod = "0",
                Nomer = "0000000000",
                Id = "Test_Serii_AK",
            };
        }

        private static TransportnyeUpakovki GenerateTestUpakovki()
        {
            return new TransportnyeUpakovki()
            {
                Uid = "Test_TransportnyeUpakovki_AK",
                ShtrihkodTransportnojUpakovki = "000000000000000000",
                ImyaTransportnojUpakovki = "Test_TransportnyeUpakovki_AK",
                IdTransportnojUpakovki = "Test_TransportnyeUpakovki_AK",
                Status = 0
            };
        }

        private static Warehouse GenerateTestWarehouse()
        {
            return new Warehouse()
            {
                Id = "000000000",
                Name = "Test_Warehouse_AK",
                StorageId = "Test_Storage_AK"
            };
        }

        private static Kontragenty GenerateTestKontragenty()
        {
            return new Kontragenty()
            {
                Uid = "Test_Kontragenty_AK",
                Naimenovanie = "Test_Kontragenty_AK",
                EtoPapka = false,
                NaimenovanieDlyaPoiska = "Test_Kontragenty_AK",
                Id = "Test_Kontragenty_AK"
            };
        }

        private static async Task<bool> RunTestMethod(string testName, Func<Task<bool>> testMethod)
        {
            bool testResult = false;

            Console.WriteLine($"Test: {testName}");
            try { testResult = await testMethod(); }
            catch (Exception ex)
            {
                testResult = false;
                logger.Error("{@ex}", ex);
            }
#if DEBUG
            Console.Write("Result: ");
            if (testResult)
                Console.ForegroundColor = ConsoleColor.Green;
            else Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(testResult);
            Console.ForegroundColor = ConsoleColor.Gray;
#else
            Console.WriteLine($"Result: {testResult}");
#endif
            Console.WriteLine();

            return testResult;
        }

        private static async Task<bool> RunTests()
        {
            Console.Clear();

            bool result = true;

            Console.WriteLine($"BaseUrl: {client.BaseUrl}");

            Console.WriteLine();
            Console.WriteLine("--Products--");

            result &= await RunTestMethod("ProductsV1_PostAsync_ProductCreated",
                async () => await clientTester.ProductsV1_PostAsync_ProductCreated(product));

            result &= await RunTestMethod("ProductsV1_DeleteByIdAsync_ProductDeleted",
                async () => await clientTester.ProductsV1_DeleteByIdAsync_ProductDeleted(product));

            Console.WriteLine("--Serii--");

            result &= await RunTestMethod("TablesV1_Post_12Async_RowCreated",
                async () => await clientTester.TablesV1_Post_12Async_RowCreated(seriiRow));

            result &= await RunTestMethod("TablesV1_DeleteByUid_12Async_RowDeleted",
                async () => await clientTester.TablesV1_DeleteByUid_12Async_RowDeleted(seriiRow));

            Console.WriteLine("--TransportnyeUpakovki--");

            result &= await RunTestMethod("TablesV1_Post_14Async_RowCreated",
                async () => await clientTester.TablesV1_Post_14Async_RowCreated(upakovkiRow));

            result &= await RunTestMethod("TablesV1_DeleteByUid_14Async_RowDeleted",
                async () => await clientTester.TablesV1_DeleteByUid_14Async_RowDeleted(upakovkiRow));

            Console.WriteLine("--Warehouses--");

            result &= await RunTestMethod("WarehousesV1_PostAsync_WarehouseCreated",
                async () => await clientTester.WarehousesV1_PostAsync_WarehouseCreated(warehouse));

            result &= await RunTestMethod("WarehousesV1_DeleteByIdAsync_WarehouseDeleted",
                async () => await clientTester.WarehousesV1_DeleteByIdAsync_WarehouseDeleted(warehouse));

            Console.WriteLine("--Kontragenty--");

            result &= await RunTestMethod("TablesV1_Post_7Async_RowCreated",
                async () => await clientTester.TablesV1_Post_7Async_RowCreated(kontragenty));

            result &= await RunTestMethod("TablesV1_DeleteByUid_7Async_RowDeleted",
                async () => await clientTester.TablesV1_DeleteByUid_7Async_RowDeleted(kontragenty));

#if DEBUG
            Console.WriteLine();
            Console.WriteLine("--Finish--");

            Console.ReadKey();
#endif
            return result;
        }

        private const int mainState = 0;
        private const int writeUrlState = 1;
        private const int runTestsState = 2;
        private const int closeAppState = 3;

        private static int ShowMainMenu()
        {
            Console.Clear();
            Console.WriteLine($"--Current url: {client.BaseUrl}--");
            Console.WriteLine($"1. Change Url");
            Console.WriteLine($"2. Run Tests");
            Console.WriteLine($"3. Exit");

            switch (Console.ReadKey().Key)
            {
                case ConsoleKey.D1:
                    return writeUrlState;
                case ConsoleKey.D2:
                    return runTestsState;
                default:
                    return closeAppState;
            }
        }

        private static int ShowWriteUrlMenu()
        {
            Console.Clear();
            Console.Write("New url: ");
            client.BaseUrl = Console.ReadLine();
            return mainState;
        }

        private static async Task<int> ShowMenu(int menuState = 0)
        {
            start:
            switch (menuState)
            {
                case mainState:
                    menuState = ShowMainMenu();
                    goto start;
                case writeUrlState:
                    menuState = ShowWriteUrlMenu();
                    goto start;
                case runTestsState:
                    return await RunTests() ? 0 : 1;
                default:
                    return 0;
            }
        }

        private static async Task<int> Main(string[] args)
        {
            return await ShowMenu();
        }
    }
}
