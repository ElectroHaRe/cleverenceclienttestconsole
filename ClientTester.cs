﻿using Cleverence;
using Serilog;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CleverenceClientTestConsole
{
    public class ClientTester
    {
        public readonly Client Client;
        public readonly ILogger Logger;

        public ClientTester(Client client)
        {
            Client = client;
        }

        public ClientTester(Client client, ILogger logger)
            : this(client)
        {
            Logger = logger;
        }

        #region Products
        public async Task<bool> ProductsV1_PostAsync_ProductCreated(Product product)
        {
            if (await CheckProductById(product.Id))
            {
                Logger?.Error($"({this.GetType()}.ProductsV1_PostAsync_ProductCreated | " +
                    $"Product id: {product.Id}) Error: The product already exists");
                return false;
            }

            await Client.ProductsV1_BeginUpdateAsync(string.Empty);
            try { await Client.ProductsV1_PostAsync(product); }
            catch (Exception ex)
            {
                Logger?.Error("({module}.ProductsV1_PostAsync_ProductCreated | " +
                    "Product id: {id}) Error: {@ex}", this.GetType(), product.Id, ex);
                return false;
            }
            finally { await Client.ProductsV1_EndUpdateAsync(string.Empty); }

            if (!await CheckProductById(product.Id))
            {
                Logger?.Error($"({this.GetType()}.ProductsV1_PostAsync_ProductCreated | " +
                    $"Product id: {product.Id}) Error: The product wasn't create");
                return false;
            }
#if DEBUG
            Logger?.Information($"{this.GetType()}.ProductsV1_PostAsync_ProductCreated | Product id: {product.Id}");
            Logger?.Information("Result: {@result}", product);
#endif
            await Client.ProductsV1_BeginUpdateAsync(string.Empty);
            try { await Client.ProductsV1_DeleteByIdAsync(product.Id, string.Empty); }
            finally { await Client.ProductsV1_EndUpdateAsync(string.Empty); }
#if DEBUG
            if (!await CheckProductById(product.Id))
                Logger?.Information("--The test product was delete");
            else Logger?.Error("Error: The test product wasn't delete.");
#endif
            return true;
        }

        public async Task<bool> ProductsV1_DeleteByIdAsync_ProductDeleted(Product product)
        {
            if (!await CheckProductById(product.Id))
            {
                await Client.ProductsV1_BeginUpdateAsync(string.Empty);
                try { await Client.ProductsV1_PostAsync(product); }
                finally { await Client.ProductsV1_EndUpdateAsync(string.Empty); }
            }

            await Client.ProductsV1_BeginUpdateAsync(string.Empty);
            try { await Client.ProductsV1_DeleteByIdAsync(product.Id, string.Empty); }
            catch (Exception ex)
            {
                Logger?.Error("({module}.ProductsV1_DeleteByIdAsync_ProductDeleted  | " +
                    "Product id: {id}) Error: {@ex}", this.GetType(), product.Id, ex);
                return false;
            }
            finally { await Client.ProductsV1_EndUpdateAsync(string.Empty); }

            if (await CheckProductById(product.Id))
            {
                Logger?.Error($"({this.GetType()}.ProductsV1_DeleteByIdAsync_ProductDeleted  |" +
                    $" Product id: {product.Id}) Error: The product wasn't delete");
                return false;
            }
#if DEBUG
            Logger?.Information($"({this.GetType()}.ProductsV1_DeleteByIdAsync_ProductDeleted  | " +
                $"Product id: {product.Id}) Result: The product was delete");
#endif
            return true;
        }

        private async Task<bool> CheckProductById(string id)
        {
#if DEBUG
            Logger?.Information($"({this.GetType()}) Check product with id: {id}");
#endif
            try { await Client.ProductsV1_GetByIdAsync(id, null, null); }
            catch (ApiException ex)
            {
                if (ex.StatusCode == 404)
                {
#if DEBUG
                    Logger?.Information("Result: doesn't exist");
#endif
                    return false;
                }
                else throw ex;
            }
            catch (Exception ex)
            {
#if DEBUG
                Logger?.Error("Error: {@ex}", ex);
#else
                Logger?.Error("({module} | Client.ProductsV1_GetByIdAsync | Product id: {id}) Error: {@ex}", this.GetType(), id, ex);
#endif
                throw ex;
            }
#if DEBUG
            Logger?.Information("Result: exists");
#endif
            return true;
        }
        #endregion

        #region Serii
        public async Task<bool> TablesV1_Post_12Async_RowCreated(Serii row)
        {
            Serii lastRow = null;
            try { lastRow = (await GetSeriiById(row.Id))?.Value.Last(); }
            catch { }

            await Client.TablesV1_BeginUpdate_12Async(string.Empty);
            try { await Client.TablesV1_Post_12Async(row); }
            catch (Exception ex)
            {
                Logger?.Error("({module}.TablesV1_Post_12Async_RowCreated | Row uid: {uid}) Error: {@ex}", this.GetType(), row.Id, ex);
                return false;
            }
            finally { await Client.TablesV1_EndUpdate_12Async(string.Empty); }

            try
            {
                row = (await GetSeriiById(row.Id)).Value.Last();
                if (row.Uid == lastRow?.Uid)
                    throw new Exception();
            }
            catch
            {
                Logger?.Error($"({this.GetType()}.TablesV1_Post_12Async_RowCreated | Row id: {row.Id}) Error: The row wasn't create");
                return false;
            }
#if DEBUG
            Logger?.Information($"{this.GetType()}.TablesV1_Post_12Async_RowCreated |  Row uid: {row.Uid}");
            Logger?.Information("Result: The row was create\n{@result}", row);
#endif
            await Client.TablesV1_BeginUpdate_12Async(string.Empty);
            try { await Client.TablesV1_DeleteByUid_12Async(GetCorrectUid(row.Uid), string.Empty); }
            finally { await Client.TablesV1_EndUpdate_12Async(string.Empty); }
#if DEBUG
            if (!await CheckSeriiByUid(row.Uid))
                Logger?.Information("--The test row was delete--");
            else Logger?.Error("Error: The test row wasn't delete.");
#endif
            return true;
        }

        public async Task<bool> TablesV1_DeleteByUid_12Async_RowDeleted(Serii row)
        {
            if ((await GetSeriiById(row.Id))?.Value.Count == 0)
            {
                await Client.TablesV1_BeginUpdate_12Async(string.Empty);
                try { await Client.TablesV1_Post_12Async(row); }
                finally { await Client.TablesV1_EndUpdate_12Async(string.Empty); }
            }

            try { row = (await GetSeriiById(row.Id)).Value.Last(); }
            catch
            {
                Logger?.Error($"({this.GetType()}.TablesV1_DeleteByUid_12Async_RowDeleted | " +
                    $"Row uid: {row.Uid}) Error: The row wasn't found");
                return false;
            }

            await Client.TablesV1_BeginUpdate_12Async(string.Empty);
            try { await Client.TablesV1_DeleteByUid_12Async(GetCorrectUid(row.Uid), string.Empty); }
            catch (Exception ex)
            {
                Logger?.Error("({module}.TablesV1_DeleteByUid_12Async_RowDeleted  | " +
                    "Row uid: {uid}) Error: {@ex}", this.GetType(), row.Uid, ex);
                return false;
            }
            finally { await Client.TablesV1_EndUpdate_12Async(string.Empty); }

            try
            {
                await Client.TablesV1_GetSubByUid_12Async(row.Uid, null, null);
                Logger?.Error($"({this.GetType()}.TablesV1_DeleteByUid_12Async_RowDeleted  | " +
                    $"Row uid: {row.Uid}) Error: The row wasn't delete");
                return false;
            }
            catch (ApiException) { }
#if DEBUG
            Logger?.Information($"({this.GetType()}.TablesV1_DeleteByUid_12Async_RowDeleted  | Row uid: {row.Uid})");
            Logger?.Information("Result: The row was delete\n{@result}", row);
#endif
            return true;
        }

        public async Task<ODataResponseOfListOfSerii> GetSeriiById(string id)
        {
            try { return await Client.TablesV1_GetSub_12Async(null, $"id eq '{id}'", null, null, null, null, null); }
            catch { return null; }
        }

        private async Task<bool> CheckSeriiByUid(string uid)
        {
            uid = GetCorrectUid(uid);
#if DEBUG
            Logger?.Information($"({this.GetType()}) Check serii with uid: {uid}");
#endif
            try { await Client.TablesV1_GetSubByUid_12Async(uid, null, null); }
            catch (ApiException ex)
            {
                if (ex.StatusCode == 404)
                {
#if DEBUG
                    Logger?.Information($"Result: Uid {uid} doesn't exist");
#endif
                    return false;
                }
                throw ex;
            }
            catch (Exception ex)
            {
#if DEBUG
                Logger?.Error("Error: {@ex}", ex);
#else
                Logger?.Error("({module} | Client.TablesV1_GetSubByUid_12Async | Row uid: {uid}) Error: {@ex}", this.GetType(), uid, ex);
#endif
                throw ex;
            }
#if DEBUG
            Logger?.Information($"Result: Uid {uid} exists");
#endif
            return true;
        }
        #endregion

        #region Warehouses
        public async Task<bool> WarehousesV1_PostAsync_WarehouseCreated(Warehouse warehouse)
        {
            if (await CheckWarehouseById(warehouse.Id))
            {
                Logger?.Error($"({this.GetType()}.WarehousesV1_PostAsync_WarehouseCreated | " +
                    $"Warehouse id: {warehouse.Id}) Error: The warehouse already exists");
                return false;
            }

            try { await TryPostWarehouseAsync(warehouse); }
            catch (Exception ex)
            {
                Logger?.Error("(module).WarehousesV1_PostAsync_WarehouseCreated | " +
                    "Warehouse id: {id}) Error: {@ex}", this.GetType(), warehouse.Id, ex);
                return false;
            }

            if (!await CheckWarehouseById(warehouse.Id))
            {
                Logger?.Error($"({this.GetType()}.WarehousesV1_PostAsync_WarehouseCreated |" +
                    $" Warehouse id: {warehouse.Id}) Error: The warehouse wasn't created");
                return false;
            }

#if DEBUG
            Logger?.Information($"{this.GetType()}.WarehousesV1_PostAsync_WarehouseCreated | Warehouse id: {warehouse.Id}");
            Logger?.Information("Result: The warehouse was create.\n{@result}", warehouse);
#endif
            await Client.WarehousesV1_DeleteByIdAsync(warehouse.Id, null);

#if DEBUG
            if (!await CheckWarehouseById(warehouse.Id))
                Logger?.Information("Error: The created warehouse wasn't delete");
#endif
            return true;
        }

        public async Task<bool> WarehousesV1_DeleteByIdAsync_WarehouseDeleted(Warehouse warehouse)
        {
            if (!await CheckWarehouseById(warehouse.Id))
                await TryPostWarehouseAsync(warehouse);

            try { await Client.WarehousesV1_DeleteByIdAsync(warehouse.Id, null); }
            catch (Exception ex)
            {
                Logger?.Information("({module}.WarehousesV1_DeleteByIdAsync_WarehouseDeleted | " +
                    "Warehouse id: {id}) Error: {@ex})", this.GetType(), warehouse.Id, ex);
                return false;
            }

            if (await CheckWarehouseById(warehouse.Id))
            {
                Logger?.Error($"({this.GetType()}).WarehousesV1_DeleteByIdAsync_WarehouseDeleted | " +
                    $"Warehouse id: {warehouse.Id}) Error: The warehouse wasn't delete");
                return false;
            }
#if DEBUG
            Logger?.Information($"({this.GetType()}.WarehousesV1_DeleteByIdAsync_WarehouseDeleted | Warehouse id: {warehouse.Id})");
            Logger?.Information("Result: The warehouse was delete\n{@result}", warehouse);
#endif
            return true;
        }

        private async Task<bool> CheckWarehouseById(string id)
        {
#if DEBUG
            Logger?.Information($"({this.GetType()}) Check warehouse with id: {id}");
#endif
            try { await Client.WarehousesV1_GetByIdAsync(id, null, null); }
            catch (ApiException ex)
            {
                if (ex.StatusCode == 404)
                {
#if DEBUG
                    Logger?.Information("Result: doesn't exist");
#endif
                    return false;
                }
                else throw ex;
            }
            catch (Exception ex)
            {
#if DEBUG
                Logger?.Error("Error: {@ex}", ex);
#else
                Logger?.Error("({module} | Client.WarehousesV1_GetByIdAsync | Warehouse id: {id}) Error: {@ex}", this.GetType(), id, ex);
#endif
                throw ex;
            }
#if DEBUG
            Logger?.Information("Result: exists");
#endif
            return true;
        }

        private async Task<Warehouse> TryPostWarehouseAsync(Warehouse warehouse)
        {
            try { return await Client.WarehousesV1_PostAsync(warehouse); }
            catch (ApiException ex)
            {
                if (ex.StatusCode == 201)
                    return warehouse;

                throw ex;
            }
        }
        #endregion

        #region TransportnyeUpakovki
        public async Task<bool> TablesV1_Post_14Async_RowCreated(TransportnyeUpakovki row)
        {
            TransportnyeUpakovki lastRow = null;
            try { lastRow = (await GetUpakovkiById(row.IdTransportnojUpakovki))?.Value.Last(); }
            catch { }

            await Client.TablesV1_BeginUpdate_14Async(string.Empty);
            try { await TryPostTransportnyeUpakovkiAsync(row); }
            catch (Exception ex)
            {
                Logger?.Error("({module}.TablesV1_Post_14Async_RowCreated | " +
                    "Row uid: {uid}) Error: {@ex}", this.GetType(), row.Uid, ex);
                return false;
            }
            finally { await Client.TablesV1_EndUpdate_14Async(string.Empty); }

            try
            {
                row = (await GetUpakovkiById(row.IdTransportnojUpakovki))?.Value.Last();
                if (row.Uid == lastRow?.Uid)
                    throw new Exception();
            }
            catch
            {
                Logger?.Information($"({this.GetType()}.TablesV1_Post_14Async_RowCreated | " +
                    $"Row id: {row.IdTransportnojUpakovki}) Error: The row wasn't create");
                return false;
            }
#if DEBUG
            Logger?.Information($"{this.GetType()}.TablesV1_Post_14Async_RowCreated | Row uid: {row.Uid}");
            Logger?.Information("Result: The row was create\n{@result}", row);
#endif
            await Client.TablesV1_BeginUpdate_14Async(string.Empty);
            try { await Client.TablesV1_DeleteByUid_14Async(GetCorrectUid(row.Uid), string.Empty); }
            finally { await Client.TablesV1_EndUpdate_14Async(string.Empty); }
#if DEBUG
            if (!await CheckUpakovkiByUid(row.Uid))
                Logger?.Information("--The test row was delete--");
            else Logger?.Error("Error: The test row wasn't delete");
#endif
            return true;
        }

        public async Task<bool> TablesV1_DeleteByUid_14Async_RowDeleted(TransportnyeUpakovki row)
        {
            if ((await GetUpakovkiById(row.IdTransportnojUpakovki))?.Value.Count == 0)
            {
                await Client.TablesV1_BeginUpdate_14Async(string.Empty);
                try { await TryPostTransportnyeUpakovkiAsync(row); }
                finally { await Client.TablesV1_EndUpdate_14Async(string.Empty); }
            }

            try { row = (await GetUpakovkiById(row.IdTransportnojUpakovki))?.Value.Last(); }
            catch
            {
                Logger?.Error($"({this.GetType()}.TablesV1_DeleteByUid_14Async_RowDeleted | " +
                    $"Row uid: {row.Uid}) Error: The row wasn't found");
                return false;
            }

            await Client.TablesV1_BeginUpdate_14Async(string.Empty);
            try { await Client.TablesV1_DeleteByUid_14Async(GetCorrectUid(row.Uid), string.Empty); }
            catch (Exception ex)
            {
                Logger?.Error("({module}.TablesV1_DeleteByUid_14Async_RowDeleted | " +
                    "Row uid: {uid}) Error: {@ex}", this.GetType(), row.Uid, ex);
                return false;
            }
            finally { await Client.TablesV1_EndUpdate_14Async(string.Empty); }

            try
            {
                await Client.TablesV1_GetSubByUid_14Async(row.Uid, null, null);
                Logger?.Error($"({this.GetType()}.TablesV1_DeleteByUid_14Async_RowDeleted | " +
                    $"Row uid: {row.Uid}) Error: The row wasn't delete");
                return false;
            }
            catch (ApiException) { }
#if DEBUG
            Logger?.Information($"({this.GetType()}.TablesV1_DeleteByUid_14Async_RowDeleted | Row uid: {row.Uid}");
            Logger?.Information("Result: The row was delete\n{@result}", row);
#endif
            return true;
        }

        public async Task<ODataResponseOfListOfTransportnyeUpakovki> GetUpakovkiById(string id)
        {
            try { return await Client.TablesV1_GetSub_14Async(null, $"IdTransportnojUpakovki eq '{id}'", null, null, null, null, null); }
            catch { return null; }
        }

        public async Task<bool> CheckUpakovkiByUid(string uid)
        {
            uid = GetCorrectUid(uid);
#if DEBUG
            Logger?.Information($"({this.GetType()}) Chek TransportnyeUpakovki with uid: {uid}");
#endif
            try { await Client.TablesV1_GetSubByUid_14Async(uid, null, null); }
            catch (ApiException ex)
            {
                if (ex.StatusCode == 404)
                {
#if DEBUG
                    Logger?.Information($"Result: Uid {uid} doesn't exist");
#endif
                    return false;
                }
                throw ex;
            }
            catch (Exception ex)
            {
#if DEBUG
                Logger?.Error("Error: {@ex}", ex);
#else
                Logger?.Error("({module} | Client.TablesV1_GetSubByUid_14Async | Row uid: {uid}) Error: {@ex}", this.GetType(), uid, ex);
#endif
                throw ex;
            }

#if DEBUG
            Logger?.Information($"Result: Uid {uid} exists");
#endif
            return true;
        }
        private async Task<TransportnyeUpakovki> TryPostTransportnyeUpakovkiAsync(TransportnyeUpakovki row)
        {
            try { return await Client.TablesV1_Post_14Async(row); }
            catch (ApiException ex)
            {
                if (ex.StatusCode == 201)
                    return row;

                throw ex;
            }
        }
        #endregion

        #region Kontragenty
        public async Task<bool> TablesV1_Post_7Async_RowCreated(Kontragenty row)
        {
            if (await CheckKotragentyByUid(row.Uid))
            {
                Logger?.Error($"({this.GetType()}.TablesV1_Post_7Async_RowCreated | " +
                    $"Row uid: {row.Uid}) Error: The row already exists");
                return false;
            }

            await Client.TablesV1_BeginUpdate_7Async(string.Empty);
            try { await TryPostKontragentyAsync(row); }
            catch (Exception ex)
            {
                await Client.TablesV1_EndUpdate_7Async(string.Empty);
                Logger?.Error("({module}.TablesV1_Post_7Async_RowCreated | " +
                    "Row uid: {uid}) Error: {@ex}", this.GetType(), row.Uid, ex);
                return false;
            }
            finally { await Client.TablesV1_EndUpdate_7Async(string.Empty); }

            if (!await CheckKotragentyByUid(row.Uid))
            {
                Logger?.Error($"({this.GetType()}.TablesV1_Post_7Async_RowCreated | " +
                    $"Row uid: {row.Uid}) Error: The row wasn't create");
                return false;
            }
#if DEBUG
            Logger?.Information($"{this.GetType()}.TablesV1_Post_7Async_RowCreated) | Row uid: {row.Uid}");
            Logger?.Information("Result: {@result}", row);
#endif
            await Client.TablesV1_BeginUpdate_7Async(string.Empty);
            try { await Client.TablesV1_DeleteByUid_7Async(row.Uid, string.Empty); }
            finally { await Client.TablesV1_EndUpdate_7Async(string.Empty); }
#if DEBUG
            if (!await CheckKotragentyByUid(row.Uid))
                Logger?.Information("--The test row was delete--");
            else Logger?.Error("Error: The test row wasn't delete");
#endif
            return true;
        }

        public async Task<bool> TablesV1_DeleteByUid_7Async_RowDeleted(Kontragenty row)
        {
            if (!await CheckKotragentyByUid(row.Uid))
            {
                await Client.TablesV1_BeginUpdate_7Async(string.Empty);
                try { await TryPostKontragentyAsync(row); }
                finally { await Client.TablesV1_EndUpdate_7Async(string.Empty); }
            }

            await Client.TablesV1_BeginUpdate_7Async(string.Empty);
            try { await Client.TablesV1_DeleteByUid_7Async(row.Uid, string.Empty); }
            catch (Exception ex)
            {
                await Client.TablesV1_EndUpdate_7Async(string.Empty);
                Logger?.Error("({module}.TablesV1_DeleteByUid_7Async_RowDeleted | " +
                    "Row uid: {uid}) Error: {@ex}", this.GetType(), row.Uid, ex);
                return false;
            }
            finally { await Client.TablesV1_EndUpdate_7Async(string.Empty); }

            if (await CheckKotragentyByUid(row.Uid))
            {
                Logger?.Error($"({this.GetType()}.TablesV1_DeleteByUid_7Async_RowDeleted | " +
                    $"Row uid: {row.Uid}) Error: The row wasn't delete");
                return false;
            }
#if DEBUG
            Logger?.Information($"({this.GetType()}.TablesV1_DeleteByUid_7Async_RowDeleted) |" +
                $"Row uid: {row.Uid}) Result: The row was delete");
#endif
            return true;
        }

        public async Task<bool> CheckKotragentyByUid(string uid)
        {
#if DEBUG
            Logger?.Information($"({this.GetType()}) Check kontragenty with uid: {uid}");
#endif
            try { await Client.TablesV1_GetSubByUid_7Async(uid, null, null); }
            catch (ApiException ex)
            {
                if (ex.StatusCode == 404)
                {
#if DEBUG
                    Logger?.Information("Result: doesn't exist");
#endif
                    return false;
                }
                else throw ex;
            }
            catch (Exception ex)
            {
#if DEBUG
                Logger?.Error("Error: {@ex}", ex);
#else
                Logger?.Error("({module}.Client.TablesV1_GetSubByUid_7Async | Row uid: {uid}) Error: {@ex}", this.GetType(), uid, ex);
#endif
                throw ex;
            }
#if DEBUG
            Logger?.Information("Result: exists");
#endif
            return true;
        }

        private async Task<Kontragenty> TryPostKontragentyAsync(Kontragenty row)
        {
            try { return await Client.TablesV1_Post_7Async(row); }
            catch (ApiException ex)
            {
                if (ex.StatusCode == 201)
                    return row;

                throw ex;
            }
        }
        #endregion

        public static string GetCorrectUid(string uidFromServer) =>
            Regex.Match(uidFromServer, @"[0-9]+$").ToString();
    }
}
