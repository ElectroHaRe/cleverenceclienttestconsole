﻿using Cleverence;
using CleverenceClientTestConsole;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Serilog;
using System;
using System.Configuration;
using System.Net.Http;
using System.Threading.Tasks;


namespace ClientTester_Tests
{
    [TestClass]
    public class WarehouseTests
    {
        private static Client client;
        private static Warehouse warehouse;
        private static ClientTester clientTester;
        private static ILogger logger;

        [ClassInitialize]
        public static void InitialSetUp(TestContext context)
        {
            logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.File("logs\\WarehouseTests.txt", rollingInterval: RollingInterval.Day)
                .CreateLogger();

            client = new Client(new HttpClient() { Timeout = TimeSpan.FromSeconds(4) });
            var configUrl = ConfigurationManager.AppSettings.Get("BaseUrl");
            client.BaseUrl = string.IsNullOrEmpty(configUrl) ? client.BaseUrl : configUrl;
            clientTester = new ClientTester(client, logger);

            warehouse = new Warehouse()
            {
                Id = "000000000",
                Name = "Test_Warehouse_AK",
                StorageId = "Test_Storage_AK"
            };
        }

        [TestInitialize]
        public void SetUp()
        {
#if DEBUG
            logger?.Information("--Test-->");
#endif
        }

        [TestMethod]
        public async Task WarehousesV1_PostAsync_WarehouseCreated_Result_True()
        {
            var result = await clientTester.WarehousesV1_PostAsync_WarehouseCreated(warehouse);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public async Task WarehousesV1_DeleteByIdAsync_WarehouseDeleted_Result_True()
        {
            var result = await clientTester.WarehousesV1_DeleteByIdAsync_WarehouseDeleted(warehouse);

            Assert.IsTrue(result);
        }
    }
}
