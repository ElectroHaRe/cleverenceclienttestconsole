﻿using Cleverence;
using CleverenceClientTestConsole;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Serilog;
using System;
using System.Configuration;
using System.Net.Http;
using System.Threading.Tasks;

namespace ClientTester_Tests
{
    [TestClass]
    public class TransportnyeUpakovkiTests
    {
        private static Client client;
        private static TransportnyeUpakovki row;
        private static ClientTester clientTester;
        private static ILogger logger;

        [ClassInitialize]
        public static void ClassSetUp(TestContext context)
        {
            logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.File("logs\\TransportnyeUpakovkiTests.txt", rollingInterval: RollingInterval.Day)
                .CreateLogger();

            client = new Client(new HttpClient() { Timeout = TimeSpan.FromMinutes(4) });
            var configUrl = ConfigurationManager.AppSettings.Get("BaseUrl");
            client.BaseUrl = string.IsNullOrEmpty(configUrl) ? client.BaseUrl : configUrl;
            clientTester = new ClientTester(client, logger);

            row = new TransportnyeUpakovki()
            {
                Uid = "Test_TransportnyeUpakovki_AK",
                ShtrihkodTransportnojUpakovki = "000000000000000000",
                ImyaTransportnojUpakovki = "Test_TransportnyeUpakovki_AK",
                IdTransportnojUpakovki = "Test_TransportnyeUpakovki_AK",
                Status = 0
            };
        }

        [ClassCleanup]
        public static async Task ClassCleanUp()
        {
            var trash = (await clientTester.GetUpakovkiById(row.IdTransportnojUpakovki)).Value;

#if DEBUG
            if (trash.Count > 0)
                logger.Information("trash: ");
#endif

            foreach (var item in trash)
            {
                await client.TablesV1_DeleteByUid_12Async(ClientTester.GetCorrectUid(item.Uid), 1.ToString());
#if DEBUG
                logger.Information("{@item}", item);
#endif
            }
        }

        [TestInitialize]
        public void SetUp()
        {
#if DEBUG
            logger?.Information("--Test-->");
#endif
        }

        [TestMethod]
        public async Task TablesV1_Post_14Async_RowCreated_Result_True()
        {
            var result = await clientTester.TablesV1_Post_14Async_RowCreated(row);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public async Task TablesV1_DeleteByUid_14Async_RowDeleted()
        {
            var result = await clientTester.TablesV1_DeleteByUid_14Async_RowDeleted(row);

            Assert.IsTrue(result);
        }
    }
}
