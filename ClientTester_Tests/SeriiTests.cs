﻿using Cleverence;
using CleverenceClientTestConsole;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Serilog;
using System;
using System.Configuration;
using System.Net.Http;
using System.Threading.Tasks;

namespace ClientTester_Tests
{
    [TestClass]
    public class SeriiTests
    {
        private static Client client;
        private static Serii row;
        private static ClientTester clientTester;
        private static ILogger logger;

        [ClassInitialize]
        public static void ClassSetUp(TestContext context)
        {
            logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.File("logs\\SeriiTests.txt", rollingInterval: RollingInterval.Day)
                .CreateLogger();

            client = new Client(new HttpClient() { Timeout = TimeSpan.FromMinutes(4) });
            var configUrl = ConfigurationManager.AppSettings.Get("BaseUrl");
            client.BaseUrl = string.IsNullOrEmpty(configUrl) ? client.BaseUrl : configUrl;
            clientTester = new ClientTester(client, logger);

            row = new Serii()
            {
                Uid = "Test_Serii_AK",
                ImyaSerii = "0000000000 до 01.01.01",
                KlyuchSerij = "a585a000-a000-00a0-aa00-00a00a0aa00a",
                DataRozliva = new DateTimeOffset?(new DateTime(2001, 1, 1, 0, 0, 0)),
                NaimenovanieDlyaPoiska = "0000000000 до 01.01.01",
                DataSerii = new DateTimeOffset?(new DateTime(2001, 1, 1, 0, 0, 0)),
                IndeksSort = 0,
                Shtrihkod = "0",
                Nomer = "0000000000",
                Id = "Test_Serii_AK",
            };
        }

        [ClassCleanup]
        public static async Task ClassCleanUp()
        {
            var trash = (await clientTester.GetSeriiById(row.Id)).Value;

#if DEBUG
            if (trash.Count > 0)
                logger.Information("trash: ");
#endif

            foreach (var item in trash)
            {
                await client.TablesV1_DeleteByUid_12Async(ClientTester.GetCorrectUid(item.Uid), 1.ToString());
#if DEBUG
                logger.Information("{@item}", item);
#endif
            }
        }

        [TestInitialize]
        public void SetUp()
        {
#if DEBUG
            logger?.Information("--Test-->");
#endif
        }

        [TestMethod]
        public async Task TablesV1_Post_12Async_RowCreated_Result_True()
        {
            var result = await clientTester.TablesV1_Post_12Async_RowCreated(row);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public async Task TablesV1_DeleteByUid_12Async_RowDeleted()
        {
            var result = await clientTester.TablesV1_DeleteByUid_12Async_RowDeleted(row);

            Assert.IsTrue(result);
        }
    }
}
