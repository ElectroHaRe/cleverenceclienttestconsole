﻿using Cleverence;
using CleverenceClientTestConsole;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Serilog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Threading.Tasks;

namespace ClientTester_Tests
{
    [TestClass]
    public class KontragentyTests
    {
        private static Client client;
        private static Kontragenty kontragenty;
        private static ClientTester clientTester;
        private static ILogger logger;

        [ClassInitialize]
        public static void InitialSetUp(TestContext context)
        {
            logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.File("logs\\ProductTests.txt", rollingInterval: RollingInterval.Day)
                .CreateLogger();

            client = new Client(new HttpClient() { Timeout = TimeSpan.FromSeconds(4) });
            var configUrl = ConfigurationManager.AppSettings.Get("BaseUrl");
            client.BaseUrl = string.IsNullOrEmpty(configUrl) ? client.BaseUrl : configUrl;
            clientTester = new ClientTester(client, logger);

            kontragenty = new Kontragenty()
            {
                Uid = "Test_Kontragenty_AK",
                Naimenovanie = "Test_Kontragenty_AK",
                EtoPapka = false,
                NaimenovanieDlyaPoiska = "Test_Kontragenty_AK",
                Id = "Test_Kontragenty_AK"
            };
        }

        [TestInitialize]
        public void SetUp()
        {
#if DEBUG
            logger?.Information("--Test-->");
#endif
        }

        [TestMethod]
        public async Task TablesV1_Post_7Async_Result_True()
        {
            var result = await clientTester.TablesV1_Post_7Async_RowCreated(kontragenty);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public async Task TablesV1_DeleteByUid_7Async_RowDeleted_Result_True()
        {
            var result = await clientTester.TablesV1_DeleteByUid_7Async_RowDeleted(kontragenty);

            Assert.IsTrue(result);
        }
    }
}
