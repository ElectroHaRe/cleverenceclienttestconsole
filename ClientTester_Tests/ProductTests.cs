﻿using Cleverence;
using CleverenceClientTestConsole;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Serilog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Threading.Tasks;

namespace ClientTester_Tests
{
    [TestClass]
    public class ProductTests
    {
        private static Client client;
        private static Product product;
        private static ClientTester clientTester;
        private static ILogger logger;

        [ClassInitialize]
        public static void InitialSetUp(TestContext context)
        {
            logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.File("logs\\ProductTests.txt", rollingInterval: RollingInterval.Day)
                .CreateLogger();

            client = new Client(new HttpClient() { Timeout = TimeSpan.FromSeconds(4) });
            var configUrl = ConfigurationManager.AppSettings.Get("BaseUrl");
            client.BaseUrl = string.IsNullOrEmpty(configUrl) ? client.BaseUrl : configUrl;
            clientTester = new ClientTester(client, logger);

            product = new Product()
            {
                Id = "00000000000000",
                Barcode = "00000000000000",
                Marking = "00000000000000",
                BasePackingId = "0000000a - 000a - 0a00 - 0000 - 00a00aaa00a0",
                Name = "Test_Product_AK",
                Packings = new List<Packing>()
                {
                    new Packing()
                    {
                        Id = "0000000a - 000a - 0a00 - 0000 - 00a00aaa00a0",
                        Name = "Test_Packing_AK",
                        UnitsQuantity = 1
                    }
                }
            };
        }

        [TestInitialize]
        public void SetUp()
        {
#if DEBUG
            logger?.Information("--Test-->");
#endif
        }

        [TestMethod]
        public async Task ProductsV1_PostAsync_ProductCreated_Result_True()
        {
            var result = await clientTester.ProductsV1_PostAsync_ProductCreated(product);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public async Task ProductsV1_DeleteByIdAsync_ProductDeleted_Result_True()
        {
            var result = await clientTester.ProductsV1_DeleteByIdAsync_ProductDeleted(product);

            Assert.IsTrue(result);
        }
    }
}
